import React from 'react';
import { createStackNavigator  } from 'react-navigation';

import HomeScreen from './screens/HomeScreen'
import DetailsScreen from './screens/DetailScreen'
import ActivityIndicatorScreen from './screens/ActivityIndicatorScreen'
import DrawerLayoutScreen from './screens/DrawerLayoutScreen'
import ImageScreen from './screens/ImageScreen'
import TextScreen from './screens/TextScreen'
import KeyboardAvoidingViewScreen  from './screens/KeyboardAvoidingViewScreen'
import ListViewScreen from './screens/ListViewScreen'
import ModalScreen from './screens/ModalScreen'
import PickerScreen from './screens/PickerScreen'
import ProgressBarAndroidScreen from './screens/ProgressBarAndroidScreen'
import RefreshControlScreen from './screens/RefreshControlScreen'
import ScrollViewScreen from './screens/ScrollViewScreen'
import SectionListScreen from './screens/SectionListScreen'
import SliderScreen from './screens/SliderScreen'
import StatusBarScreen from './screens/StatusBarScreen'
import SwitchScreen from './screens/SwitchScreen'
import TextInputScreen from './screens/TextInputScreen'
import TouchableHighlightScreen from './screens/TouchableHighlightScreen'
import TouchableNativeFeedbackScreen from './screens/TouchableNativeFeedbackScreen'
import TouchableOpacityScreen from './screens/TouchableOpacityScreen'
import ViewPagerScreen from './screens/ViewPagerScreen'
import ViewScreen from './screens/ViewScreen'
import WebViewScreen from './screens/WebViewScreen'


const RootStack = createStackNavigator (
  {
    Home: {
      screen: HomeScreen,
    }, 
    Details: {
      screen: DetailsScreen,
    },
    ActivityIndicator: {
      screen: ActivityIndicatorScreen,
    },
    DrawerLayout: {
      screen: DrawerLayoutScreen,
    },
    ImageScreen:{
      screen: ImageScreen,
    },
    TextScreen:{
      screen: TextScreen,
    },
    KeyboardAvoid: {
      screen: KeyboardAvoidingViewScreen,
    },
    ListView: {
      screen: ListViewScreen,
    },
    ModalScreen: {
      screen: ModalScreen,
    },
    PickerScreen: {
      screen: PickerScreen,
    },
    ProgressBar: {
      screen: ProgressBarAndroidScreen,
    },
    RefreshControl: {
      screen: RefreshControlScreen,
    },
    ScrollView: {
      screen: ScrollViewScreen,
    },
    SectionList: {
      screen: SectionListScreen,
    },   
    Slider: {
      screen: SliderScreen,
    },
    StatusBar: {
      screen: StatusBarScreen,
    }, 
    Switch: {
      screen: SwitchScreen,
    },
    TextInput: {
      screen: TextInputScreen,
    },
    TouchableHighlight: {
      screen: TouchableHighlightScreen,
    },
    TouchableNativeFeedback: {
      screen: TouchableNativeFeedbackScreen,
    },
    TouchableOpacity: {
      screen: TouchableOpacityScreen,
    },
    ViewPager: {
      screen: ViewPagerScreen,
    },
    View: {
      screen: ViewScreen,
    },
    WebView: {
      screen: WebViewScreen,
    },

  },

  
  {
    initialRouteName: 'Home',
  }
);


export default class App extends React.Component {
  render() {
    return <RootStack/>;
  }
}
