import React, { Component } from 'react'
import {
    StyleSheet,
    TouchableNativeFeedback,
    Text,
    View,
    Button
} from 'react-native'

export default class TouchableNativeFeedbackScreen extends Component {
    constructor(props) {
        super(props)
        this.state = { count: 0 }
    }

    onPress = () => {
        this.setState({
            count: this.state.count + 1
        })
    }

    render() {
        return (
            <View style={styles.container}>
                <TouchableNativeFeedback
                    style={styles.button}
                    onPress={this.onPress}
                >
                    <Text> Touch Here </Text>
                </TouchableNativeFeedback>
                <View style={[styles.countContainer]}>
                    <Text style={[styles.countText]}>
                        {this.state.count !== 0 ? this.state.count : null}
                    </Text>
                </View>
                <Button style={styles.space}
                    title="Home"
                    onPress={() => this.props.navigation.navigate('Home')}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        paddingHorizontal: 10
    },
    button: {
        alignItems: 'center',
        backgroundColor: '#DDDDDD',
        padding: 10
    },
    countContainer: {
        alignItems: 'center',
        padding: 10
    },
    countText: {
        color: '#FF00FF'
    }
});
