import React, { Component } from 'react';
import {ActivityIndicator,StyleSheet,Text,View,Button} from 'react-native';

class ActivityIndicatorScreen extends Component {
    render() {
        return (
            <View style={[styles.container]}>
            <Text style={styles.title} >Activity Indicator Screen BOI</Text>
            <ActivityIndicator size="large" color="#0000ff" />
            <ActivityIndicator size="small" color="#00ff00" />
            <ActivityIndicator size="large" color="#0000ff" />
            <ActivityIndicator size="small" color="#00ff00" />
            <Button title="Home"onPress={() => this.props.navigation.navigate('Home')}/>
        </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    title: {
      fontSize: 20,
      margin: 20,
      textAlign: 'center',
    },
  }) 

export default ActivityIndicatorScreen;