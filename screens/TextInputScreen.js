import React, { Component } from 'react';
import { View, TextInput, Button, StyleSheet } from 'react-native';

class UselessTextInput extends Component {
  render() {
    return (
      <TextInput
        {...this.props}
        editable = {true}
        maxLength = {40}
      />
    );
  }
}

export default class TextInputScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: 'Useless Multiline Placeholder',
    };
  }

  render() {
    return (
     <View style={{
       backgroundColor: this.state.text,
       borderBottomColor: '#000000',
       borderBottomWidth: 1 }}
     >
       <UselessTextInput
         multiline = {true}
         numberOfLines = {4}
         onChangeText={(text) => this.setState({text})}
         value={this.state.text}
       />
       <Button style={styles.space}
            title="Home"
            onPress={() => this.props.navigation.navigate('Home')}
        />
     </View>
    );
  }
}

const styles = StyleSheet.create({
  space: {
    margin: 20,
  }
});