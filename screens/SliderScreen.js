import React, { Component } from 'react';
import {    
  StyleSheet,
  Text,
  View,
  Slider,
  Button
} from 'react-native';

export default class SliderScreen extends Component {
  constructor(props) {
   super(props)
   this.state = { age: 18 }
  } 
  getVal(val){
  console.warn(val);
  }     
  render() {    

    return (
      <View style={styles.container}>
        <Slider
         style={{ width: 300 }}
         step={1}
         minimumValue={0}
         maximumValue={100}
         value={this.state.age}
         onValueChange={val => this.setState({ age: val })}
         onSlidingComplete={ val => this.getVal(val)}
        />
        <Text style={styles.welcome}>
          {this.state.age}
        </Text>
        <Button style={styles.space}
            title="Home"
            onPress={() => this.props.navigation.navigate('Home')}
        />          
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  space: {
    margin: 20,
  },
});
