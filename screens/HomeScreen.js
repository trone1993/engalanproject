import React, { Component } from 'react';
import { StyleSheet, Text, View, Button, ScrollView } from 'react-native';
import TextScreen from './TextScreen';

class HomeScreen extends Component {
    render() {
        return (
          <ScrollView>
          <View style={styles.container}>
          
            <Button
              title="Go to Details"
              onPress={() => this.props.navigation.navigate('Details')}
            />

            <Button style={styles.space}
              title="Activity Indicator"
              onPress={() => this.props.navigation.navigate('ActivityIndicator')}
            />

            <Button style={styles.space}
              title="Drawer Layout"
              onPress={() => this.props.navigation.navigate('DrawerLayout')}
            />
            
            <Button style={styles.space}
              title="Image Screen"
              onPress={() => this.props.navigation.navigate('ImageScreen')}
            />

            <Button style={styles.space} 
              title="TextScreen"
              onPress={() => this.props.navigation.navigate('TextScreen')}
            />
            
            <Button style={styles.space}
              title="Keyboard Avoid Screen"
              onPress={() => this.props.navigation.navigate('KeyboardAvoid')}
            />

            <Button style={styles.space}
              title="List View"
              onPress={() => this.props.navigation.navigate('ListView')}
            />

            <Button style={styles.space}
              title="Modal"
              onPress={() => this.props.navigation.navigate('ModalScreen')}
            />

            <Button style={styles.space}
              title="Picker Screen"
              onPress={() => this.props.navigation.navigate('PickerScreen')}
            />

            <Button style={styles.space}
              title="Progress Bar"
              onPress={() => this.props.navigation.navigate('ProgressBar')}
            />

            <Button style={styles.space}
              title="Refresh Control"
              onPress={() => this.props.navigation.navigate('RefreshControl')}
            />

            <Button style={styles.space}
              title="Scroll View"
              onPress={() => this.props.navigation.navigate('ScrollView')}
            />

            <Button style={styles.space}
              title="Section List"
              onPress={() => this.props.navigation.navigate('SectionList')}
            />
            <Button style={styles.space}
              title="Slider"
              onPress={() => this.props.navigation.navigate('Slider')}
            />

            <Button style={styles.space}
              title="Status Bar"
              onPress={() => this.props.navigation.navigate('StatusBar')}
            />

            <Button style={styles.space}
              title="Switch Screen"
              onPress={() => this.props.navigation.navigate('Switch')}
            />

            <Button style={styles.space}
              title="Text Input"
              onPress={() => this.props.navigation.navigate('TextInput')}
            />

            <Button style={styles.space}
              title="Touchable HighLight"
              onPress={() => this.props.navigation.navigate('TouchableHighlight')}
            />

            <Button style={styles.space}
              title="Touchable Native Feedback"
              onPress={() => this.props.navigation.navigate('TouchableNativeFeedback')}
            />

            <Button style={styles.space}
              title="Touchable Opacity"
              onPress={() => this.props.navigation.navigate('TouchableOpacity')}
            />

            <Button style={styles.space}
              title="View Pager Android"
              onPress={() => this.props.navigation.navigate('ViewPager')}
            />

            <Button style={styles.space}
              title="View Screen"
              onPress={() => this.props.navigation.navigate('View')}
            />

            <Button style={styles.space}
              title="Web View"
              onPress={() => this.props.navigation.navigate('WebView')}
            />
          </View>
          </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    title: {
      fontSize: 20,
      margin: 20,
      textAlign: 'center',
    },
    space: {
      margin: 20,
    }

  });
  
export default HomeScreen;