import React, { Component } from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

class DetailsScreen extends Component {
    render() {
        return (
          <View style={styles.container}>
            <Text style={styles.title} >Details Screen
            IT fvcking Alive</Text>
            <Button
              title="Home"
              onPress={() => this.props.navigation.navigate('Home')}
            />
          </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    title: {
      fontSize: 20,
      margin: 20,
      textAlign: 'center',
    },
  }
);

export default DetailsScreen;